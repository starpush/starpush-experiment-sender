package net.starpush.android.sender;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.PowerWhitelistManager;

public class TestHelper {
    @SuppressLint("WrongConstant")
    public static void sendMessage(Context context) {
        PowerWhitelistManager pwm;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            pwm = context.getSystemService(PowerWhitelistManager.class);
        } else {
            pwm = (PowerWhitelistManager) context.getSystemService("power_whitelist");
        }

        pwm.whitelistAppTemporarily("net.starpush.android.sender", 10_000);

        Intent intent = new Intent();
        intent.setPackage("net.starpush.android.receiver");
        intent.setAction("net.starpush.android.receiver.action.RECEIVER");
        context.sendBroadcast(intent);
    }
}
